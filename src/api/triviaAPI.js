export function getQuestions(amount, category, difficulty, type) {
    let apiUrl = `https://opentdb.com/api.php?amount=${amount}`
    if (category) apiUrl+= `&category=${category}`
    if (difficulty) apiUrl+= `&difficulty=${difficulty}`
    if (type) apiUrl+= `&type=${type}`
    let count = 1

    return fetch(apiUrl)
      .then(response => {
          if (response.status !== 200) {
              throw new Error('Could not fetch the trivia question!')
          }
            return response.json()        
      })
      .then(response => {
          return response.results.map(question => ({
              ...question,
              //id: Math.random().toString(24).slice(2),
              id: count++,
              givenAnswer: "",
              answers: [...question.incorrect_answers, question.correct_answer]
          }))
      })
}

  