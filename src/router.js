import Vue from 'vue'
import VueRouter from 'vue-router'
import TriviaSettings from './components/TriviaSettings.vue'
import TriviaQuestions from './components/TriviaQuestions.vue'
import TriviaResult from './components/TriviaResult.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        alias: '/start',
        component: TriviaSettings
    },
    {
        path: '/questions',
        component: TriviaQuestions
    },
    {
        path: '/result',
        component: TriviaResult
    }
]

export default new VueRouter({ routes })
